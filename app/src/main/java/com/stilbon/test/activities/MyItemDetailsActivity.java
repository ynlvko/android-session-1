package com.stilbon.test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.squareup.picasso.Picasso;
import com.stilbon.test.R;
import com.stilbon.test.pojos.MyItem;

public class MyItemDetailsActivity extends AppCompatActivity implements CompoundButton.OnCheckedChangeListener {
    public static final String EXTRA_ITEM       = "extra_item";
    public static final String EXTRA_ITEM_NAME  = "extra_item_name";
    public static final String EXTRA_ITEM_IMAGE = "extra_item_image";

    private ToggleButton mTbChangeState;
    private TextView     mTvName;
    private ImageView    mIvImage;
    private View         mScrollView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_item_details);

        assignFields();
        fillFields();
    }

    private void fillFields() {
        Intent intent = getIntent();

        MyItem item = (MyItem) intent.getSerializableExtra(EXTRA_ITEM);

        String name = intent.getStringExtra(EXTRA_ITEM_NAME);
        String image = intent.getStringExtra(EXTRA_ITEM_IMAGE);

        mTvName.setText(name);
        Picasso.with(this).load(image).into(mIvImage);
    }

    private void assignFields() {
        mTbChangeState = (ToggleButton) findViewById(R.id.tbChangeState);
        mTvName = (TextView) findViewById(R.id.tvName);
        mIvImage = (ImageView) findViewById(R.id.ivImage);
        mScrollView = findViewById(R.id.scrollView);

        mTbChangeState.setOnCheckedChangeListener(this);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Animation a;
        if (isChecked) {
            a = new AlphaAnimation(1, 0);
        } else {
            a = new AlphaAnimation(0, 1);
        }
        a.setDuration(1500);
        a.setFillAfter(true);
        mScrollView.setAnimation(a);
        mScrollView.animate();
    }
}
