package com.stilbon.test.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.stilbon.test.R;
import com.stilbon.test.adapters.MyListAdapter;
import com.stilbon.test.network.NetworkManager;
import com.stilbon.test.pojos.MyItem;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MyListActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    private ListView mLvItems;

    private MyListAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_list);

        mAdapter = new MyListAdapter();

        mLvItems = (ListView) findViewById(R.id.lvItems);
        mLvItems.setAdapter(mAdapter);
        mLvItems.setOnItemClickListener(this);

        NetworkManager.get().getItems(new Callback<List<MyItem>>() {
            @Override
            public void success(List<MyItem> myItems, Response response) {
                mAdapter.setItems(myItems);
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(MyListActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        MyItem item = mAdapter.getItem(position);
        Intent intent = new Intent(this, MyItemDetailsActivity.class);

        intent.putExtra(MyItemDetailsActivity.EXTRA_ITEM, item);

        intent.putExtra(MyItemDetailsActivity.EXTRA_ITEM_NAME, item.getName());
        intent.putExtra(MyItemDetailsActivity.EXTRA_ITEM_IMAGE, item.getImage());

        startActivity(intent);
    }
}
