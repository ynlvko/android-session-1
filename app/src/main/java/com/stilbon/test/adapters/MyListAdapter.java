package com.stilbon.test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.stilbon.test.R;
import com.stilbon.test.pojos.MyItem;

import java.util.ArrayList;
import java.util.List;

public class MyListAdapter extends BaseAdapter {
    private List<MyItem> mItems = new ArrayList<>();

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public MyItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder h;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.my_list_item_view, parent, false);

            h = new ViewHolder(convertView);
            convertView.setTag(h);
        } else {
            h = (ViewHolder) convertView.getTag();
        }

        h.bind(parent.getContext(), getItem(position));

        return convertView;
    }

    public void setItems(List<MyItem> myItems) {
        mItems = myItems;
        notifyDataSetChanged();
    }

    private static class ViewHolder {
        TextView tvTitle;
        ImageView ivImage;

        public ViewHolder(View v) {
            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
            ivImage = (ImageView) v.findViewById(R.id.ivImage);
        }

        public void bind(Context context, MyItem item) {
            tvTitle.setText(item.getName());
            Picasso.with(context).load(item.getImage()).into(ivImage);
        }
    }
}
