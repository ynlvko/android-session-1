package com.stilbon.test.network;

import com.stilbon.test.pojos.MyItem;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;

public class NetworkManager {
    private static NetworkManager sInstance;

    private NetworkService mService;

    private NetworkManager() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("http://dev.tapptic.com")
                .build();

        mService = restAdapter.create(NetworkService.class);
    }

    public static NetworkManager get() {
        synchronized (NetworkManager.class) {
            if (sInstance == null) {
                sInstance = new NetworkManager();
            }
            return sInstance;
        }
    }

    public void getItems(Callback<List<MyItem>> callback) {
        mService.getItems(callback);
    }
}
