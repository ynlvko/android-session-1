package com.stilbon.test.network;

import com.stilbon.test.pojos.MyItem;

import java.util.List;

import retrofit.Callback;
import retrofit.http.GET;

public interface NetworkService {
    @GET("/test/json.php")
    void getItems(Callback<List<MyItem>> callback);
}
