package com.stilbon.test.pojos;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import lombok.Data;
import lombok.experimental.Accessors;

@Data @Accessors(prefix = "m")
public class MyItem implements Serializable {
    @SerializedName("name") private  String mName;
    @SerializedName("image") private String mImage;
}
